// Copyright 2019 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';

import 'package:digital_clock/weather.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clock_helper/model.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:weather/weather.dart';

import 'number.dart';
import 'time_of_day_gradient.dart';

class DigitalClock extends StatefulWidget {
  const DigitalClock(this.model);

  final ClockModel model;

  @override
  _DigitalClockState createState() => _DigitalClockState();
}

class _DigitalClockState extends State<DigitalClock>
    with SingleTickerProviderStateMixin {
  static const animationDurationMs = 750;
  DateTime _dateTime = DateTime.now();
  Timer _timer;

  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: animationDurationMs),
        vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeInOut)
      ..addStatusListener((status) {
        if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });
    controller.forward();
    _updateTime();
  }

  @override
  void didUpdateWidget(DigitalClock oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.model != oldWidget.model) {}
  }

  @override
  void dispose() {
    _timer?.cancel();
    controller.dispose();
    widget.model.dispose();
    super.dispose();
  }

  void _updateTime() {
    setState(() {
      controller.reset();
      _dateTime = DateTime.now();
      var duration = Duration(seconds: 1) -
          Duration(milliseconds: _dateTime.millisecond) -
          Duration(milliseconds: animationDurationMs);
      // if we are already animating to the correct number, set timer for next second
      if (duration.inMilliseconds < 0) {
        duration = duration + Duration(seconds: 1);
      }
      _timer = Timer(duration, _updateTime);
    });
  }

  @override
  Widget build(BuildContext context) {
    var lightTheme = Theme.of(context).brightness == Brightness.light;
    var weather = WeatherInfo(widget.model.location);
    double scale = widget.model.scale;

    var bottomTextStyle = TextStyle(
      color: lightTheme ? Colors.white : Colors.white.withAlpha(150),
      fontSize: (MediaQuery.of(context).size.width * scale / 35)
          .round()
          .ceilToDouble(),
      letterSpacing: 1.5,
      fontWeight: FontWeight.w300,
    );

    var clock = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Center(
          child: AnimatedClock(
            animation: animation,
            from: _dateTime,
            to: _dateTime.add(Duration(seconds: 1)),
            color: Colors.white,
            hour24: widget.model.is24HourFormat,
            scale: scale,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: (MediaQuery.of(context).size.width * scale) / 40,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    DateFormat("EEEE, MMM d").format(_dateTime),
                    style: bottomTextStyle,
                  ),
                  weatherIcon(weather, bottomTextStyle.fontSize),
                  temperature(weather, bottomTextStyle),
                ],
              ),
            ],
          ),
        ),
      ],
    );

    if (lightTheme) {
      return Container(
        color: Color(0xFF2D2E2F),
        child: AnimatedContainer(
          decoration: BoxDecoration(
            gradient: backgroundColors.elementAt(_dateTime.hour),
          ),
          duration: Duration(milliseconds: animationDurationMs),
          child: clock,
        ),
      );
    } else {
      return Container(
        color: Color(0xFF121212),
        child: clock,
      );
    }
  }

  Widget temperature(WeatherInfo weather, TextStyle style) {
    return Container(
      constraints: BoxConstraints(minWidth: 120, maxWidth: 200),
      child: weather.builder(
        onData: (Weather w) {
          var temp = widget.model.unit == TemperatureUnit.fahrenheit
              ? w.temperature.fahrenheit
              : w.temperature.celsius;
          return Text(
            temp.round().toString() + widget.model.unitString,
            style: style,
          );
        },
      ),
    );
  }

  Widget weatherIcon(WeatherInfo weather, double height) {
    if (kIsWeb) {
      // web currently doesnt support drawing SVGs, nor does it support tinting Images
      return Container();
    }
    return weather.iconAsset(
      onData: (assetName) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 14),
          child: SvgPicture.asset(
            assetName,
            color: Colors.white,
            height: height,
            width: height,
            fit: BoxFit.fill,
          ),
        );
      },
    );
  }
}
