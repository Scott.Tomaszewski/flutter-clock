import 'dart:ui';

import 'package:flutter/material.dart';

// Gradients representing each hour of the day, zero-indexed
// Color source: https://codepen.io/zessx/details/rDEAl
final backgroundColors = [
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF00000c), Color(0xFF00000c)],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF020111), Color(0xFF191621)],
    stops: [.85, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF020111), Color(0xFF20202c)],
    stops: [.60, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF020111), Color(0xFF3a3a52)],
    stops: [.10, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF20202c), Color(0xFF515175)],
    stops: [0, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF40405c), Color(0xFF6f71aa), Color(0xFF8a76ab)],
    stops: [0, .80, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF4a4969), Color(0xFF7072ab), Color(0xFFcd82a0)],
    stops: [0, .50, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF757abf), Color(0xFF8583be), Color(0xFFeab0d1)],
    stops: [0, .60, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF82addb), Color(0xFFebb2b1)],
    stops: [0, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF94c5f8), Color(0xFFa6e6ff), Color(0xFFb1b5ea)],
    stops: [1, .70, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFFb7eaff), Color(0xFF94dfff)],
    stops: [0, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF9be2fe), Color(0xFF67d1fb)],
    stops: [0, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF90dffe), Color(0xFF38a3d1)],
    stops: [0, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF57c1eb), Color(0xFF246fa8)],
    stops: [0, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF2d91c2), Color(0xFF1e528e)],
    stops: [0, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF2473ab), Color(0xFF1e528e), Color(0xFF5b7983)],
    stops: [0, .70, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF1e528e), Color(0xFF265889), Color(0xFF9da671)],
    stops: [0, .50, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF1e528e), Color(0xFF728a7c), Color(0xFFe9ce5d)],
    stops: [0, .50, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF154277), Color(0xFF576e71), Color(0xFFe1c45e), Color(0xFFb26339)],
    stops: [0, .30, .70, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
      Color(0xFF163C52),
      Color(0xFF4F4F47),
      Color(0xFFC5752D),
      Color(0xFFB7490F),
      Color(0xFF2F1107)
    ],
    stops: [0, .30, .60, .80, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF071B26), Color(0xFF071B26), Color(0xFF8A3B12), Color(0xFF240E03)],
    stops: [0, .30, .80, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF010A10), Color(0xFF59230B), Color(0xFF2F1107)],
    stops: [.30, .80, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF090401), Color(0xFF4B1D06)],
    stops: [.50, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF00000c), Color(0xFF150800)],
    stops: [.80, 1.00],
  ),
  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xFF00000c), Color(0xFF00000c)],
  ),
];
