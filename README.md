# Flutter Clock

Digital Number-tweening clock designed by Scott Tomaszewski for the Flutter Clock Challenge
(https://flutter.dev/clock)


A (limited, see Limitations section) live, web view can be found at
(https://scott.tomaszewski.gitlab.io/flutter-clock).

#### Sample

![Tweening clock](flutter_clock.gif "Tweening Digital Flutter Clock")


## Features

- Digital clock with number tweening animations.
- Background color reflective of the current hour of the day (light-mode).
- 12-hour and 24-hour support.
- Simple date, temperature, and weather support.
- Simple dark mode for night time.

## Limitations

- Weather icons unavailable in web version due to
    - Inability to load SVG images (https://github.com/dnfield/flutter_svg/issues/173)
    - Inability to tint Images (https://github.com/flutter/flutter/issues/33316)

## Attribution

- Weather Icons: https://github.com/erikflowers/weather-icons
    - Weather Icons licensed under [SIL OFL 1.1](http://scripts.sil.org/OFL)
    - Code licensed under [MIT License](http://opensource.org/licenses/mit-license.html)
    - Documentation licensed under [CC BY 3.0](http://creativecommons.org/licenses/by/3.0)
- Sky gradient background colors: https://codepen.io/zessx/details/rDEAl
> Copyright (c) 2020 by zessx (https://codepen.io/zessx/pen/rDEAl)
>
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
- Number Tweening
    - Reference: https://sriramramani.wordpress.com/2013/10/14/number-tweening/
    - Reference: Timely (https://play.google.com/store/apps/details?id=ch.bitspin.timely)
    - Samples of number curves: https://github.com/adnan-SM/TimelyTextView
> Copyright 2014 Adnan A M.
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
>
>    http://www.apache.org/licenses/LICENSE-2.0
>
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.


# Original README contents

Welcome to Flutter Clock!

See [flutter.dev/clock](https://flutter.dev/clock) for how to get started, submission requirements, contest rules, and FAQs.

See a [live demo](https://maryx.github.io/flutter_clock) with Flutter for Web!

Example [Analog Clock](analog_clock)

<img src='analog_clock/analog.gif' width='350'>

Example [Digital Clock](digital_clock)

<img src='digital_clock/digital.gif' width='350'>
