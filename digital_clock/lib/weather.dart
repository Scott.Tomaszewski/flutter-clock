import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:weather/weather.dart';

class WeatherInfo {
  static final WeatherInfo _instance = WeatherInfo._init();

  WeatherInfo._init();

  factory WeatherInfo(String cityName) {
    _instance._cityName = cityName;
    return _instance;
  }

  final WeatherFactory wf = WeatherFactory('4efa9563c867ce8a1a0cdf5267fe40c0');
  final Duration secondsBetweenRequests = Duration(seconds: 30);

  DateTime _lastRequest;
  Weather _last;
  String _cityName;

  Future<Weather> update() async {
    if (_lastRequest == null ||
        DateTime.now().isAfter(_lastRequest.add(secondsBetweenRequests))) {
      _last = await wf.currentWeatherByCityName(_cityName);
      _lastRequest = DateTime.now();
    }
    return _last;
  }

  Weather get() {
    update();
    return _last;
  }

  Future<Widget> currentIcon(double height) async {
    if (kIsWeb) {
      // web currently doesnt support drawing SVGs, nor does it support tinting Images
      return Container();
    }

    var w = await update();
    var iconCode = w.weatherIcon;

    var iconCodeToImage = {
      "01d.png": "wi-day-sunny",
      "01n.png": "wi-night-clear",
      "02d.png": "wi-day-sunny",
      "02n.png": "wi-night-clear",
      "03d.png": "wi-day-cloudy",
      "03n.png": "wi-night-alt-cloudy",
      "04d.png": "wi-day-cloudy",
      "04n.png": "wi-night-alt-cloudy",
      "09d.png": "wi-day-rain",
      "09n.png": "wi-night-alt-rain",
      "10d.png": "wi-day-rain",
      "10n.png": "wi-night-alt-rain",
      "11d.png": "wi-day-storm-showers",
      "11n.png": "wi-night-alt-storm-showers",
      "13d.png": "wi-day-snow",
      "13n.png": "wi-night-alt-snow",
      "50d.png": "wi-day-fog",
      "50n.png": "wi-night-fog",
    };

    if (iconCodeToImage.containsKey(iconCode)) {
      var image = iconCodeToImage[iconCode];
      return SvgPicture.asset(
        "assets/svg/" + image + ".svg",
        color: Colors.white,
        height: height,
        fit: BoxFit.fill,
      );
    } else {
      return Container();
    }
  }

  var iconCodeToImage = {
    "01d": "wi-day-sunny",
    "01n": "wi-night-clear",
    "02d": "wi-day-sunny",
    "02n": "wi-night-clear",
    "03d": "wi-day-cloudy",
    "03n": "wi-night-alt-cloudy",
    "04d": "wi-day-cloudy",
    "04n": "wi-night-alt-cloudy",
    "09d": "wi-day-rain",
    "09n": "wi-night-alt-rain",
    "10d": "wi-day-rain",
    "10n": "wi-night-alt-rain",
    "11d": "wi-day-storm-showers",
    "11n": "wi-night-alt-storm-showers",
    "13d": "wi-day-snow",
    "13n": "wi-night-alt-snow",
    "50d": "wi-day-fog",
    "50n": "wi-night-fog",
  };

  Widget iconAsset(
      {Widget noData, Widget unknownIcon, Function(String) onData}) {
    return builder(
      noData: noData,
      onData: (w) {
        String iconCode = w.weatherIcon;
        if (iconCodeToImage.containsKey(iconCode)) {
          var image = iconCodeToImage[iconCode];
          return onData("assets/svg/" + image + ".svg");
        } else {
          return unknownIcon == null ? Container() : unknownIcon;
        }
      },
    );
  }

  FutureBuilder builder({Widget noData, Function(Weather) onData}) {
    return FutureBuilder(
      future: this.update(),
      builder: (context, snapshot) {
        var w = snapshot.connectionState == ConnectionState.done
            ? snapshot.data
            : _last;

        if (w == null) {
          return noData == null ? CircularProgressIndicator() : noData;
        }
        return onData(w);
      },
    );
  }
}
