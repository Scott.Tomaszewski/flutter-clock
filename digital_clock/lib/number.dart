import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

/// Clock widget that tweens numbers.
class AnimatedClock extends AnimatedWidget {
  /// Previous date to tween from.
  final DateTime from;

  /// Next date to tween to.
  final DateTime to;

  /// Indicates whether to display in 24-hour format or 12-hour.
  final bool hour24;

  /// Color used to paint the numbers.
  final Color color;

  final double scale;

  AnimatedClock({
    Key key,
    Animation<double> animation,
    this.from,
    this.to,
    this.color = Colors.white,
    this.hour24 = false,
    this.scale = 1,
  }) : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    var amPm = Text(
      DateFormat('a').format(to),
      style: TextStyle(
        color: color.withAlpha(200),
        fontSize: (MediaQuery.of(context).size.width * scale) / 20,
        fontWeight: FontWeight.w200,
      ),
    );
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _animated(_hour(from), _hour(to), context, false),
        Text(
          ":",
          style: TextStyle(
            color: this.color,
            fontSize: (MediaQuery.of(context).size.width * scale) / 15,
          ),
        ),
        _animated(_minute(from), _minute(to), context, false),
        Container(
          height: (MediaQuery.of(context).size.width * scale) / 7,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              this.hour24 ? Container() : amPm,
              _animated(_second(from), _second(to), context, true),
            ],
          ),
        ),
      ],
    );
  }

  /// Helper to build a tween form of one integer to another.  Returns a smaller version if [small].
  AnimatedInt _animated(int from, int to, BuildContext ctx, bool small) {
    var digitHeight = (MediaQuery.of(ctx).size.width * scale) / 6;

    var style = Paint()
      ..color = this.color
      ..strokeWidth = digitHeight / 50
      ..strokeCap = StrokeCap.butt
      ..isAntiAlias = true
      ..strokeJoin = StrokeJoin.round
      ..style = PaintingStyle.stroke;

    if (small) {
      digitHeight = digitHeight / 2.8;
      style = Paint()
        ..color = this.color
        ..strokeWidth = digitHeight / 80
        ..strokeCap = StrokeCap.butt
        ..isAntiAlias = true
        ..strokeJoin = StrokeJoin.round
        ..style = PaintingStyle.stroke;
    }

    // Remove 20% of width to avoid monospace nature of square canvas.
    var digitSize = Size(digitHeight * 0.8, digitHeight);
    final Animation<double> animation = listenable;
    return AnimatedInt(
      from: from,
      to: to,
      digitSize: digitSize,
      leftPad: 2,
      animation: animation,
      style: style,
    );
  }

  int _hour(DateTime dt) {
    var format = this.hour24 ? DateFormat('HH') : DateFormat('hh');
    return int.parse(format.format(dt));
  }

  int _minute(DateTime dt) {
    return int.parse(DateFormat('mm').format(dt));
  }

  int _second(DateTime dt) {
    return int.parse(DateFormat('ss').format(dt));
  }
}

/// Drawn integer that tweens between numbers.
class AnimatedInt extends AnimatedWidget {
  /// Previous int to tween from.
  final int from;

  /// Next int to tween to.
  final int to;

  /// Size of the canvas for each digit in the int.
  final Size digitSize;

  /// Number of digits to left-pad with zeros.
  final int leftPad;

  /// Style to use for painting each digit.
  final Paint style;

  AnimatedInt({
    Key key,
    Animation<double> animation,
    this.from,
    this.to,
    this.digitSize,
    this.leftPad,
    this.style,
  }) : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;

    final fromStr = from.toString();
    final toStr = to.toString();
    final fromDigits = fromStr
        .padLeft(leftPad, '0')
        .split('')
        .map((char) => int.parse(char))
        .toList();
    final toDigits = toStr
        .padLeft(leftPad, '0')
        .split('')
        .map((char) => int.parse(char))
        .toList();

    var digits = List<Widget>();
    for (var i = 0; i < leftPad; i++) {
      digits.add(AnimatedDigit(
        from: fromDigits.elementAt(i),
        to: toDigits.elementAt(i),
        animation: animation,
        digitSize: digitSize,
        style: style,
      ));
    }

    return Row(children: digits);
  }
}

/// Drawn digit that tweens between numbers.
class AnimatedDigit extends AnimatedWidget {
  /// Previous digit to tween from.  Digits over 9 have modulus applied to them.
  final int from;

  /// Next digit to tween to.  Digits over 9 have modulus applied to them.
  final int to;

  /// Size of the canvas for each digit in the int.
  final Size digitSize;

  /// Style to use for painting each digit.
  final Paint style;

  final List<SegmentedChar> numbers = [
    zero,
    one,
    two,
    three,
    four,
    five,
    six,
    seven,
    eight,
    nine
  ];

  AnimatedDigit(
      {Key key,
      Animation<double> animation,
      this.from,
      this.to,
      this.digitSize,
      this.style})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    final from = numbers.elementAt(this.from % 10);
    final to = numbers.elementAt(this.to % 10);
    return CustomPaint(
        size: digitSize,
        painter: SegmentedCharPainter(from, to, animation.value, style));
  }
}

/// Painter capable of creating a canvas of tweened [SegmentedChar] at some [progress] percentage (0-1).
class SegmentedCharPainter extends CustomPainter {
  /// SegmentedChar to tween from.
  final SegmentedChar first;

  /// SegmentedChar to tween to.
  final SegmentedChar second;

  /// Percentage of progress of the tween.
  final double progress;

  /// Style to use for painting.
  final Paint style;

  SegmentedCharPainter(this.first, this.second, this.progress, this.style);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawPath(first.transitionTo(second, progress).asPath(size), style);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

/// Value object that represents a character broken up into cubic curves and an origin point.
class SegmentedChar {
  /// Starting point of the character.
  final Offset origin;

  /// Ordered list of relative [Segments] to build the character from the [origin].
  final List<Segment> segments;

  SegmentedChar(this.origin, this.segments);

  /// Creates a full [Path] representing the character.
  Path asPath(Size size) {
    // For all dx, shift left 20%, then multiply by 125% to stretch to full width (remove monospace)
    // (.2 - 0.2) * 1.25 = 0, (1 - 0.2) * 1.25 = 1

    var p = Path();
    p.moveTo((origin.dx - 0.2) * size.width * 1.25, origin.dy * size.height);

    for (var segment in segments) {
      p.cubicTo(
        (segment.control1.dx - 0.2) * size.width * 1.25,
        segment.control1.dy * size.height,
        (segment.control2.dx - 0.2) * size.width * 1.25,
        segment.control2.dy * size.height,
        (segment.end.dx - 0.2) * size.width * 1.25,
        segment.end.dy * size.height,
      );
    }

    return p;
  }

  /// Creates a new [SegmentedChar] object created from tweening this [SegmentedChar] to the [dest]
  /// [SegmentedChar] as evaluated at a certain [progress] percentation (0-1).
  SegmentedChar transitionTo(SegmentedChar dest, double progress) {
    var newOrigin = transition(origin, dest.origin, progress);
    var newSegs = List<Segment>();

    for (var i = 0; i < segments.length; i++) {
      newSegs.add(segments[i].transitionTo(dest.segments[i], progress));
    }
    return SegmentedChar(newOrigin, newSegs);
  }
}

/// A simplified API and value object representing the cubic curve parameters for [Path_cubicTo].
class Segment {
  /// First cubic control point, params x1 and y1.
  final Offset control1;

  /// Second cubic control point, params x2, y2.
  final Offset control2;

  /// Curve end point, params x3, y3.
  final Offset end;

  Segment(this.control1, this.control2, this.end);

  /// Creates a new [Segment] created from tweening this [Segment] to the [dest] [Segment] as
  /// evaluated at a certain [progress] percentage (0-1).
  Segment transitionTo(Segment dest, double progress) {
    return Segment(
      transition(control1, dest.control1, progress),
      transition(control2, dest.control2, progress),
      transition(end, dest.end, progress),
    );
  }
}

/// Creates a new [Offset] created from tweening the [begin] [Offset] to the [end] [Offset]
/// evaluated at a certain [transitionProgress] percentage (0-1).
Offset transition(Offset begin, Offset end, double transitionProgress) {
  if (begin == null || end == null) {
    return null;
  }
  return Offset(begin.dx + ((end.dx - begin.dx) * transitionProgress),
      begin.dy + ((end.dy - begin.dy) * transitionProgress));
}

/// [SegmentedChar] representing a "1"
SegmentedChar one =
    SegmentedChar(Offset(0.425414364640884, 0.113259668508287), [
  Segment(
    Offset(0.425414364640884, 0.113259668508287),
    Offset(0.577348066298343, 0.113259668508287),
    Offset(0.577348066298343, 0.113259668508287),
  ),
  Segment(
    Offset(0.577348066298343, 0.113259668508287),
    Offset(0.577348066298343, 1),
    Offset(0.577348066298343, 1),
  ),
  Segment(
    Offset(0.577348066298343, 1),
    Offset(0.577348066298343, 1),
    Offset(0.577348066298343, 1),
  ),
  Segment(
    Offset(0.577348066298343, 1),
    Offset(0.577348066298343, 1),
    Offset(0.577348066298343, 1),
  ),
]);

/// [SegmentedChar] representing a "2"
SegmentedChar two = SegmentedChar(Offset(0.30939226519337, 0.331491712707182), [
  Segment(
    Offset(0.325966850828729, 0.0110497237569061),
    Offset(0.790055248618785, 0.0220994475138122),
    Offset(0.798342541436464, 0.337016574585635),
  ),
  Segment(
    Offset(0.798342541436464, 0.430939226519337),
    Offset(0.718232044198895, 0.541436464088398),
    Offset(0.596685082872928, 0.674033149171271),
  ),
  Segment(
    Offset(0.519337016574586, 0.762430939226519),
    Offset(0.408839779005525, 0.856353591160221),
    Offset(0.314917127071823, 0.977900552486188),
  ),
  Segment(
    Offset(0.314917127071823, 0.977900552486188),
    Offset(0.812154696132597, 0.977900552486188),
    Offset(0.812154696132597, 0.977900552486188),
  ),
]);

/// [SegmentedChar] representing a "3"
SegmentedChar three =
    SegmentedChar(Offset(0.361878453038674, 0.298342541436464), [
  Segment(
    Offset(0.348066298342541, 0.149171270718232),
    Offset(0.475138121546961, 0.0994475138121547),
    Offset(0.549723756906077, 0.0994475138121547),
  ),
  Segment(
    Offset(0.861878453038674, 0.0994475138121547),
    Offset(0.806629834254144, 0.530386740331492),
    Offset(0.549723756906077, 0.530386740331492),
  ),
  Segment(
    Offset(0.87292817679558, 0.530386740331492),
    Offset(0.828729281767956, 0.994475138121547),
    Offset(0.552486187845304, 0.994475138121547),
  ),
  Segment(
    Offset(0.298342541436464, 0.994475138121547),
    Offset(0.30939226519337, 0.828729281767956),
    Offset(0.312154696132597, 0.790055248618785),
  ),
]);

/// [SegmentedChar] representing a "4"
SegmentedChar four =
    SegmentedChar(Offset(0.856353591160221, 0.806629834254144), [
  Segment(
    Offset(0.856353591160221, 0.806629834254144),
    Offset(0.237569060773481, 0.806629834254144),
    Offset(0.237569060773481, 0.806629834254144),
  ),
  Segment(
    Offset(0.237569060773481, 0.806629834254144),
    Offset(0.712707182320442, 0.138121546961326),
    Offset(0.712707182320442, 0.138121546961326),
  ),
  Segment(
    Offset(0.712707182320442, 0.138121546961326),
    Offset(0.712707182320442, 0.806629834254144),
    Offset(0.712707182320442, 0.806629834254144),
  ),
  Segment(
    Offset(0.712707182320442, 0.806629834254144),
    Offset(0.712707182320442, 0.988950276243094),
    Offset(0.712707182320442, 0.988950276243094),
  ),
]);

/// [SegmentedChar] representing a "5"
SegmentedChar five =
    SegmentedChar(Offset(0.806629834254144, 0.110497237569061), [
  Segment(
    Offset(0.502762430939227, 0.110497237569061),
    Offset(0.502762430939227, 0.110497237569061),
    Offset(0.502762430939227, 0.110497237569061),
  ),
  Segment(
    Offset(0.397790055248619, 0.430939226519337),
    Offset(0.397790055248619, 0.430939226519337),
    Offset(0.397790055248619, 0.430939226519337),
  ),
  Segment(
    Offset(0.535911602209945, 0.364640883977901),
    Offset(0.801104972375691, 0.469613259668508),
    Offset(0.801104972375691, 0.712707182320442),
  ),
  Segment(
    Offset(0.773480662983425, 1.01104972375691),
    Offset(0.375690607734807, 1.0939226519337),
    Offset(0.248618784530387, 0.850828729281768),
  )
]);

/// [SegmentedChar] representing a "6"
SegmentedChar six =
    SegmentedChar(Offset(0.607734806629834, 0.110497237569061), [
  Segment(
    Offset(0.607734806629834, 0.110497237569061),
    Offset(0.607734806629834, 0.110497237569061),
    Offset(0.607734806629834, 0.110497237569061),
  ),
  Segment(
    Offset(0.392265193370166, 0.43646408839779),
    Offset(0.265193370165746, 0.50828729281768),
    Offset(0.25414364640884, 0.696132596685083),
  ),
  Segment(
    Offset(0.287292817679558, 1.13017127071823),
    Offset(0.87292817679558, 1.06077348066298),
    Offset(0.845303867403315, 0.696132596685083),
  ),
  Segment(
    Offset(0.806629834254144, 0.364640883977901),
    Offset(0.419889502762431, 0.353591160220994),
    Offset(0.295580110497238, 0.552486187845304),
  ),
]);

/// [SegmentedChar] representing a "7"
SegmentedChar seven =
    SegmentedChar(Offset(0.259668508287293, 0.116022099447514), [
  Segment(
    Offset(0.259668508287293, 0.116022099447514),
    Offset(0.87292817679558, 0.116022099447514),
    Offset(0.87292817679558, 0.116022099447514),
  ),
  Segment(
    Offset(0.87292817679558, 0.116022099447514),
    Offset(0.66666666666667, 0.41068139962),
    Offset(0.66666666666667, 0.41068139962),
  ),
  Segment(
    Offset(0.66666666666667, 0.41068139962),
    Offset(0.460405157, 0.7053406998),
    Offset(0.460405157, 0.7053406998),
  ),
  Segment(
    Offset(0.460405157, 0.7053406998),
    Offset(0.25414364640884, 1),
    Offset(0.25414364640884, 1),
  ),
]);

/// [SegmentedChar] representing a "8"
SegmentedChar eight =
    SegmentedChar(Offset(0.558011049723757, 0.530386740331492), [
  Segment(
    Offset(0.243093922651934, 0.524861878453039),
    Offset(0.243093922651934, 0.104972375690608),
    Offset(0.558011049723757, 0.104972375690608),
  ),
  Segment(
    Offset(0.850828729281768, 0.104972375690608),
    Offset(0.850828729281768, 0.530386740331492),
    Offset(0.558011049723757, 0.530386740331492),
  ),
  Segment(
    Offset(0.243093922651934, 0.530386740331492),
    Offset(0.198895027624309, 0.988950276243094),
    Offset(0.558011049723757, 0.988950276243094),
  ),
  Segment(
    Offset(0.850828729281768, 0.988950276243094),
    Offset(0.850828729281768, 0.530386740331492),
    Offset(0.558011049723757, 0.530386740331492),
  ),
]);

/// [SegmentedChar] representing a "9"
SegmentedChar nine =
    SegmentedChar(Offset(0.80939226519337, 0.552486187845304), [
  Segment(
    Offset(0.685082872928177, 0.751381215469613),
    Offset(0.298342541436464, 0.740331491712707),
    Offset(0.259668508287293, 0.408839779005525),
  ),
  Segment(
    Offset(0.232044198895028, 0.0441988950276243),
    Offset(0.81767955801105, -0.0441988950276243),
    Offset(0.850828729281768, 0.408839779005525),
  ),
  Segment(
    Offset(0.839779005524862, 0.596685082872928),
    Offset(0.712707182320442, 0.668508287292818),
    Offset(0.497237569060773, 0.994475138121547),
  ),
  Segment(
    Offset(0.497237569060773, 0.994475138121547),
    Offset(0.497237569060773, 0.994475138121547),
    Offset(0.497237569060773, 0.994475138121547),
  ),
]);

/// [SegmentedChar] representing a "0"
SegmentedChar zero =
    SegmentedChar(Offset(0.24585635359116, 0.552486187845304), [
  Segment(
    Offset(0.24585635359116, 0.331491712707182),
    Offset(0.370165745856354, 0.0994475138121547),
    Offset(0.552486187845304, 0.0994475138121547),
  ),
  Segment(
    Offset(0.734806629834254, 0.0994475138121547),
    Offset(0.861878453038674, 0.331491712707182),
    Offset(0.861878453038674, 0.552486187845304),
  ),
  Segment(
    Offset(0.861878453038674, 0.773480662983425),
    Offset(0.734806629834254, 0.994475138121547),
    Offset(0.552486187845304, 0.994475138121547),
  ),
  Segment(
    Offset(0.370165745856354, 0.994475138121547),
    Offset(0.24585635359116, 0.773480662983425),
    Offset(0.24585635359116, 0.552486187845304),
  ),
]);
